const assert = require('assert')
const Chromozome = require('../Application/Chromozome')

const _ = require('lodash')

describe('Chromozome', () => {
    describe('get fitness()', () => {
        it('doit renvoyer un entier supérieur à 0 si le trajet contient au moins deux villes.', () => {
            let c = new Chromozome()
            c.trajet.push(
                {coordonnees : {
                    "lan": 48.854885,
                    "lng": 2.338646
                }},
                {coordonnees: {
                    "lan": 50.608719,
                    "lng": 3.063295
                }}
            )
            
            assert.notEqual(c.getFitness(), 0, 'Le trajet ne fait pas 0 km.')
        })

        it('doit renvoyer un nombre', () => {
            let c = new Chromozome()

            c.trajet.push(
                {coordonnees : {
                    "lan": 48.854885,
                    "lng": 2.338646
                }},
                {coordonnees: {
                    "lan": 50.608719,
                    "lng": 3.063295
                }}
            )
            assert.equal(_.isNaN(c.getFitness()), false)
        })
    })
})