const assert = require('assert')
const Generation = require('../Application/Generation')
const Chromozome = require('../Application/Chromozome')
const _ = require('lodash')

const config = require('./config.js')

describe('Generation', () => {
    describe('newGeneration()', () => {
        it('doit renvoyer un certain nombre d\'éléments', () => {
            const g = new Generation()

            g.newGeneration()

            assert.equal(g.chromozomes.length, config.NB_CHROMOZOME_PAR_GENERATION, `Il y a une différence de taille entre la taille actuelle (${g.chromozomes.length}) et celle attendu (${config.NB_CHROMOZOME_PAR_GENERATION}).`)
        })
    })

    describe('generateRandomChromozome()', () => {
        it('ne doit pas renvoyer deux fois le même objet', () => {
            const g = new Generation()

            const c1 = g.generateRandomChromozome()
            const c2 = g.generateRandomChromozome()

            assert.notEqual(c1, c2, 'Les deux instances sont égales.')
        })
    })

    describe('selection()', () => {
        it('doit renvoyer le bon nombre d\'éléments', () => {
            const g = new Generation()

            g.newGeneration()
            g.selection()

            const nbAttendu = config.NB_CHROMOZOME_PAR_GENERATION * (config.POURCENTAGE_PARENTS_A_GARDER_PAR_GENERATION / 100)

            assert.equal(g.chromozomes.length, nbAttendu, `Ne renvoie pas le bon nombre d'éléments.Actual: ${g.chromozomes.length} ; Attendu: ${nbAttendu}`)
        })
        it('doit renvoyer les meilleurs éléments', () => {
            const g = new Generation()

            g.newGeneration()

            const sorted = _.sortBy(g.chromozomes, [(c) => c.fitness])
            const best = sorted.slice(0, sorted.length * (config.POURCENTAGE_PARENTS_A_GARDER_PAR_GENERATION /100))

            g.selection()

            const fitnessDeSelection = g.chromozomes.map(c => c.fitness).join(', ')
            const fitnessDeBest = best.map( c => c.fitness).join(', ')

            assert.equal(
                fitnessDeSelection,
                fitnessDeBest,
                `Actuel: ${fitnessDeSelection} ; expected: ${fitnessDeBest}`)
        })
    })

    describe('evolution()', () => {
        it('doit renvoyer une population complète', () => {
            const g = new Generation()

            g.newGeneration()
            g.selection()
            g.evolution()

            assert.equal(g.chromozomes.length, config.NB_CHROMOZOME_PAR_GENERATION, 'La population n\'est pas au complet.')
        })
    })

    describe('croisement()', () => {
        it('doit renvoyer un chromozome avec un trajet de N villes.', () => {
            const a = new Generation().generateRandomChromozome()
            const b = new Generation().generateRandomChromozome()

            const c = new Generation().croisement(a,b)

            assert.equal(c.trajet.length, config.NB_VILLE_PAR_TRAJET)
        })
        it('doit toujours renvoyer des chromozomes avec un trajet de N villes.', () => {
            const g = new Generation()

            g.newGeneration()
            g.selection()
            g.evolution()

            g.chromozomes.forEach( (chromozome) => {
                assert.equal(chromozome.trajet.length, config.NB_VILLE_PAR_TRAJET, `Il y a ${chromozome.trajet.length} villes dans le trajet au lieu de ${config.NB_VILLE_PAR_TRAJET}.`)
            })
        })
    })
})
