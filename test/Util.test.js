const assert = require('assert')
const Util = require('../Application/Utils')
const rand = require('chance').Chance()

describe('Util', () => {
    
    describe('getPremierQuart()', () => {
        it('doit renvoyer le bon nombre d\'éléments', () => {
            const a = new Array(rand.integer({min: 0, max: 100}))
            const nbElem = a.length
            const quart = Math.round(nbElem/4)
            const n = Util.getPremierQuart(a)

            assert.equal(n.length, quart)
        })
    })

    describe('getDernierQuart()', () => {
        it('doit renvoyer le bon nombre paire d\'éléments', () => {
            const a = new Array(20)
            const n = Util.getDernierQuart(a)

            assert.equal(n.length, 5)
        })

        it('doit renvoyer le bon nombre impaire d\'éléments', () => {
            const a = new Array(11)
            const n = Util.getDernierQuart(a)

            assert.equal(n.length, 3)
        })
    })

    describe('getDernierQuart()', () => {
        it('doit renvoyer le bon nombre aléatoire d\'éléments', () => {
            const a = new Array(rand.integer({min: 1, max: 100}))
            const nbElem = a.length
            const quart = Math.round(nbElem/4)
            const n = Util.getDernierQuart(a)

            assert.equal(n.length, quart)
        })
    })
    /
    describe('getElementNotIn()', () => {
        it('doit renvoyer les bons elements', ()=> {
            const a = [ 1, 2, 6 ,7 ,8]
            const b = [ 1, 3, 4 ,7 ,9]

            const r = Util.getElementNotIn(b,a)

            assert(r, [3, 4, 9], 'Mauvaises valeurs')
        })
        it('doit renvoyer le bon nombre d\'éléments', () => {
            const a = [1, 4]
            const b = [3, 1, 2]

            const reste = Util.getElementNotIn(b, a)

            const doitRenvoyer = 2
            assert.equal(reste.length, doitRenvoyer, 'Renvoie pas le bon nb d\'éléments')
        })
    })

    describe('getPremiereMoitie()', () => {
        it('doit renvoyer la moitié des éléments', () => {
            const a = new Array(rand.integer({min: 1, max: 100}))

            const b = Util.getPremiereMoitie(a)

            assert.equal(b.length, Math.round(a.length / 2))
        })

        it('doit renvoyer les bons éléments', () => {
            const a = [1,2,3,4,5,6,7,8,9,0]
            const b = Util.getPremiereMoitie(a)
            assert.equal(b.toString(), [1,2,3,4,5].toString())
        })
    })

})