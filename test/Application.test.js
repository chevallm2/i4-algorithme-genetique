const assert = require('assert')

const Application = require('../Application/Application')

describe('Application', () => {
    describe('getNewGeneration()', () => {
        it('ne doit pas renvoyer null ou undefined', () => {
            const App = new Application()

            for (let i = 0; i < 5; i++) {
                const g = App.getNewGeneration()
                assert.notEqual(g, null, 'Pas ouf...')            
            }
        })
    })
})