module.exports = {
    NB_CHROMOZOME_PAR_GENERATION: 50,
    POURCENTAGE_PARENTS_A_GARDER_PAR_GENERATION: 50,
    NB_VILLE_PAR_TRAJET: 15,
    /* Méthodes :
     * 1) pere(0.25) + mere(0.5) + pere(0.25)
     * 2) pere(0.5) + mere(0.5)
     * 
    */
    METHODE_CROISEMENT: 2
}