const ctxG = $("#graph")

let ctxM = document.getElementById('map').getContext('2d')
const elem = document.getElementById('map')
const elemLeft = elem.offsetLeft
const elemTop = elem.offsetTop
let img = new Image()
img.src = 'france.png'
img.onload = () => {
    ctxM.drawImage(img, 0, 0)
}

const villes = [
    {
        nom: "Paris",
        x: 193,
        y: 127
    },
    {
        nom: "Lille",
        x: 213,
        y: 49
    },
    {
        nom: "Strasbourg",
        x: 326,
        y: 122
    },
    {
        nom: "Lyon",
        x: 258,
        y: 229
    },
    {
        nom: "Nice",
        x: 319,
        y: 304
    },
    {
        nom: "Marseille",
        x: 273,
        y: 315
    },
    {
        nom: "Montpellier",
        x: 233,
        y: 310
    },
    {
        nom: "Toulouse",
        x: 166,
        y: 312
    },
    {
        nom: "Pau",
        x: 120,
        y: 320
    },
    {
        nom: "Bordeaux",
        x: 116,
        y: 263
    },
    {
        nom: "Nantes",
        x: 96,
        y: 174
    },
    {
        nom: "Brest",
        x: 26,
        y: 124
    },
    {
        nom: "Tours",
        x: 155,
        y: 170
    },
    {
        nom: "Clermont-Ferrand",
        x: 214,
        y: 230
    },
    {
        nom: "Limoges",
        x: 166,
        y: 227
    }
]

$(document).ready( () => {

    let playing = false
    const nbSeconde = 0.1
    const vitesse = nbSeconde * 1000
    let bestChromozome = {}
    
    
    var scatterChart = new Chart(ctxG, {
        type: 'scatter',
        data: {
            datasets: [],
            backgroundColor: []
        },
        options: {
            legend: {
                display: false
            },
            animation: { duration: 0 },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    },
                    type: 'linear',
                    position: 'bottom'
                }],
                yAxes: [{
                    ticks: {
                         beginAtZero: false,
                         min: 3000
                        },
                }]
            }
        }
    });
    
    $('#play').click( () => {
        playing = true
    })

    $('#pause').click( () => {
        playing = false
    })
    
    $("#nouvelleGeneration").click( (e) => {
        $.get('/new')
        .catch( error => console.error(error))
        .done( result => {
            console.log(`${result.generation.filter( c => c.mutant == true).length} in this gen. (${result.numero})`)
            if(bestChromozome != result.best) {
                console.log('Nouveau meilleur chromozome')
                dessinerTrajet(result.best.trajet)
                bestChromozome = result.best
            }
            alimenterEncart(result)
            scatterChart.data.datasets.push(creerUnDataset(result.numero, result.generation))
            scatterChart.update()
        })      
    })

    
    setInterval( () => {
        if(playing) {
            $.get('/new')
            .catch( error => console.error(error))
            .done( result => {
                console.log(`${result.generation.filter( c => c.mutant == true).length} in this gen. (${result.numero})`)
                if(bestChromozome != result.best) {
                    dessinerTrajet(result.best.trajet)
                    bestChromozome = result.best
                }
                alimenterEncart(result)
                scatterChart.data.datasets.push(creerUnDataset(result.numero, result.generation))
                scatterChart.update()
            }) 
        }
    }, vitesse  )

    $("#map").click( (event) => {
        console.log('click @')
        console.log(event.pageX - elemLeft, event.pageY - elemTop)
    })
    
})

function creerUnDataset(numeroDeGeneration, generation) {
    const retour = {}
    retour.label = numeroDeGeneration + "G"
    retour.data = generation.map( (chromozome) => {
        return { x: numeroDeGeneration, y: chromozome.fitness}
    })
    retour.backgroundColor = generation.map( (chromozome) => {
        if(chromozome)
        if(chromozome.mutant) {
            return "rgba(46, 204, 113,1.0)"
        } else {
            return "rgba(231, 76, 60,1.0)"
        }
    })
    retour.borderColor = generation.map( (chromozome) => {
        if(chromozome.mutant) {
            return "rgba(39, 174, 96,1.0)"
        } else {
            return "rgba(192, 57, 43,1.0)"
        }
    })
    return retour
}

function alimenterEncart(res) {
    $('#ngen').text(res.numero)
    $('#best-km').text(res.best.fitness + " Km")
    $('#best-trajet').text(res.best.trajet.map( v => v.city).join(' → '))
    $('#pop').text(res.population)
    $('#ngenC').text(res.best.generation)
}


function dessinerTrajet(trajet) {

    ctxM.clearRect(0,0,400,400)
    ctxM.drawImage(img, 0, 0)

    
    const passage = trajet.map( (ville) => {
        return villes.find( (villeA) => villeA.nom == ville.city)
    })

    ctxM.beginPath()
    for (let i = 0; i < passage.length; i++) {
        try {
            ctxM.moveTo(passage[i].x, passage[i].y)
            ctxM.lineTo(passage[i+1].x, passage[i+1].y)        
            ctxM.stroke()
        } catch (error) {
            
        }
    }
    
}



