const _ = require('lodash')
module.exports = {

    getDistance: function(point1, point2) {
        const e = (3.14159265358979 * point1.lan / 180)
        const f = (3.14159265358979 * point1.lng / 180)
        const g = (3.14159265358979 * point2.lan / 180)
        const h = (3.14159265358979 * point2.lng / 180)
        const i =
        (
            Math.cos(e)*Math.cos(g)*Math.cos(f)*Math.cos(h)
            + Math.cos(e)*Math.sin(f)*Math.cos(g)*Math.sin(h)
            + Math.sin(e)*Math.sin(g)
        )
        const j = Math.acos(i)
        const distanceEnKilometres = Math.round(6371*j)      
        const distanceEnMetres = distanceEnKilometres * 1000
        return distanceEnKilometres
    },

    getPremierQuart: function(array) {
        return array.slice(0, Math.round(array.length * 0.25))
    },

    getDernierQuart: function(array) {
        return array.slice(Math.round(array.length * 0.75), array.length)
    },
    
    getElementNotIn: function(arrayToTest, values) {
        /*
        console.log(arrayToTest.map( m => m.city))
        console.log('diff')
        console.log(values.map( m => m.city))
        console.log( '= >')
        console.log(_.difference(arrayToTest, values).map( m => m.city))
        */
        return _.difference(arrayToTest, values)
    },

    getPremiereMoitie: function(array) {
        const moitie = Math.round(array.length / 2)
        return array.slice(0 , moitie)
    }

}