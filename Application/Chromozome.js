const Utils = require('./Utils')

module.exports = class Chromozome {
    constructor() {
        this.trajet = new Array()
    }

    init(trajet, mutant=false, gen) {
        this.trajet = trajet
        this.fitness = this.getFitness()
        this.mutant = mutant
        this.generation = gen
    }

    getFitness() {
        let fitness = 0
        for (let i = 0; i < this.trajet.length; i++) {
            const villeActuelle = this.trajet[i]
            const villeSuivante = this.trajet[i+1]
            try {
                const distanceEntreLesVilles = Utils.getDistance(villeActuelle.coordonnees, villeSuivante.coordonnees)
                fitness += distanceEntreLesVilles
            } catch (error) {
                fitness += 0
            }
        }
        return fitness
    }

}