const Chromozome = require('./Chromozome')
const fs = require('fs')
const _ = require('lodash')
const Utils = require('./Utils')

const config = require('../config')
const cities = JSON.parse(fs.readFileSync('./cities.json', 'utf-8'))

module.exports = class Generation {

    constructor(num) {
        this.chromozomes = new Array()
        this.numero = num
    }

    newGeneration() {
        const nbChromozomeParPopulation = config.NB_CHROMOZOME_PAR_GENERATION
        for (let i = 0; i < nbChromozomeParPopulation; i++) {
            this.chromozomes.push(this.generateRandomChromozome())            
        }
    }
    
    generateRandomChromozome() {
        let chromozome = new Chromozome()
        chromozome.init(_.shuffle(cities), false, this.numero)
        return chromozome
    }

    fromLatest(lastGeneration) {
        this.chromozomes = lastGeneration.chromozomes
        return this
    }

    selection() {
        // 1. Trier les chromozomes par score de fitness
        const tries = _.sortBy(this.chromozomes, [(chromozome) => chromozome.fitness])
        // 2. Déterminer le nombre de parents à garder
        const nbParentsAGarder = this.chromozomes.length * (config.POURCENTAGE_PARENTS_A_GARDER_PAR_GENERATION / 100)
        // 3. Ne garder que ce nombre de chromozomes
        this.chromozomes = tries.slice(0, nbParentsAGarder)
    }

    evolution() {
        // 1. Déterminer le nombre d'enfants à créer
        const nbEnfantACreer = config.NB_CHROMOZOME_PAR_GENERATION - this.chromozomes.length
        // 2. Créer une boucle
        let enfants = new Array()
        for(let i = 0; i < nbEnfantACreer; i++) {
            // a. Sélection d'un père
            const pere = this.generateRandomChromozome()
            // b. Sélection d'une mère (différente du père)
            let mere = null
            do {
                mere = this.generateRandomChromozome()
            } while (mere == pere)
            // c. Déterminer si la mutation a lieu
            const mutation = Math.round(_.random(1,1000)) === 1 // 0.1%
            // d. Créer un enfant
            const enfant = this.croisement(pere, mere, mutation)
            enfants.push(enfant)
        }
        
        this.chromozomes = new Array().concat(enfants, this.chromozomes)            
    }

    croisement(pere, mere, mutation) {
        let c = new Chromozome()
        if(config.METHODE_CROISEMENT === 1) {
            // 1. Extraction du 1er 1/4 des villes
            const premier25Pourcents = Utils.getPremierQuart(pere.trajet)
            // 2. Extraction du dernier 1/4 des villes
            const dernier25Pourcents = Utils.getDernierQuart(pere.trajet)
            // 3. Extraction des villes de la mère
            const moitie = Utils.getElementNotIn(mere.trajet, _.concat(premier25Pourcents, dernier25Pourcents))        
            // 4. Création du chromozome
            c.init(_.concat(premier25Pourcents, moitie, dernier25Pourcents), false, this.numero)
        } else if(config.METHODE_CROISEMENT === 2) {
            // 1. Extraction de la première moitié du père
            const pereADN = Utils.getPremiereMoitie(pere.trajet)
            // 2. Extraction du reste par la mère
            const mereADN = Utils.getElementNotIn(mere.trajet, pereADN)
            // 3. Création du chromozome
            c.init(_.concat(pereADN,mereADN), false, this.numero)
        }
        
        if(mutation) {
            c.init(_.shuffle(c.trajet), true, this.numero)
        }
        return c
    }

    getRandomChromozome() {
        return this.chromozomes[_.random(0, this.chromozomes.length - 1)]
    }

    sort() {
        return this.chromozomes = _.sortBy(this.chromozomes, [c => c.fitness])
    }

    getMutants() {
        return this.chromozomes.filter( c => c.mutant)
    }

}

