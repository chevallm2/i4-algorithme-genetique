const Generation = require('./Generation')

module.exports = class Application {

    constructor() {
        this.generations = new Array()
    }

    getNewGeneration() {
        let newGeneration = null
        const n = this.generations.length
        if(this.generations.length === 0) {
            newGeneration = new Generation(n)
            newGeneration.newGeneration()
        } else {
            const lastGeneration = this.getLastGeneration()
            newGeneration = new Generation(n).fromLatest(lastGeneration)
            newGeneration.selection()
            newGeneration.evolution()
        }
        return newGeneration
    }

    addGeneration(generation) {
        this.generations.push(generation)
    }

    getLastGeneration() {
        return this.generations[this.generations.length-1] 
    }
}