const express = require('express')
const fs = require('fs')
const Application = require('./Application/Application')
const path = require('path')

const cities = JSON.parse(fs.readFileSync('cities.json', 'utf-8'))

const app = express()
const AlgorithmeGenetique = new Application()

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug')

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res, next) => {
    res.render('index')
})

app.get('/new', (req, res, next) => {
    AlgorithmeGenetique.addGeneration(AlgorithmeGenetique.getNewGeneration())
    const retour = {
        generation: AlgorithmeGenetique.getLastGeneration().sort(),
        numero: AlgorithmeGenetique.generations.length,
        best: AlgorithmeGenetique.getLastGeneration().chromozomes[0],
        population: AlgorithmeGenetique.getLastGeneration().chromozomes.length,
        mutants: AlgorithmeGenetique.getLastGeneration().getMutants()
    }
    res.send(retour)
})


app.listen(3000, () => console.log('App listening on port 3000'))